# Task 1 solution

### Part 1 test cases

- Verify response status to be `200`
- Verify response content type header to be `text/plain; charset=utf-8`
- Verify that `username` key is present and is of type "string"
- Verify that `password` key is present and is longer than 5 characters
- Verify that `targetUrl` key is present 

### Part 2 test cases

- Verify that `ip` key is present
- Verify that `token` key is present 
- Verify that `ip` value is valid IP address

### Setup

Before running the tests, make sure that you have working NodeJS (at least v18.4.0) and npm (at least v8.12.1) installation. Using [NVM](https://github.com/nvm-sh/nvm) to install and manage NodeJS versions is recommended.

### Installing dependencies

To install project dependencies, inside repo run:

```shell
npm install
``` 

### Running tests

To run all tests execute:

```shell
npm test
```

To run each spec file separately:
```shell
npm run part1
```

or 

```shell
npm run part2
```