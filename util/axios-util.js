import axios from 'axios';

export const Axios = {
    get: async function (url) {
        try {
            return await axios({
                method: 'GET',
                url: url,
            });
        } catch (error) {
            throw new Error(`Axios.get failed`);
        }
    },

    post: async function (url, body, authentication) {
        try {
            return await axios({
                method: 'POST',
                url: url,
                auth: authentication,
                data: body,
            });
        } catch (error) {
            throw new Error(`Axios.post failed`);
        }
    },
};
