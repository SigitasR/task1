import { Axios } from '../util/axios-util.js';

describe('Part 1 test suite', function () {
    beforeAll(async function () {
        this.response = await Axios.get('http://ptsv2.com/t/fu807-1554722621/post');
    });

    it('should verify response status code', function () {
        expect(this.response.status).toEqual(200);
    });

    it('should verify response content type', function () {
        expect(this.response.headers.getContentType()).toEqual('text/plain; charset=utf-8');
    });

    it('should verify that "username" key with string value is present in the response', function () {
        expect(typeof this.response.data.username).toEqual('string');
    });

    it('should verify that "password" key is present in the response and has value longer than 5', function () {
        expect(this.response.data.password.length).toBeGreaterThan(5);
    });

    it('should verify that "targetUrl" key is present in the response', function () {
        expect(Object.keys(this.response.data)).toContain('targetUrl');
    });
});
