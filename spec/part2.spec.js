import { Axios } from '../util/axios-util.js';
import { isIPv4 } from 'net';

describe('Part 2 test suite', function () {
    beforeAll(async function () {
        const getResponse = await Axios.get('http://ptsv2.com/t/fu807-1554722621/post');
        const { username, password, targetUrl } = getResponse.data;
        this.postResponseData = (await Axios.post(targetUrl, {}, { password: password, username: username })).data;
    });

    it('should verify that "ip" key is present', function () {
        expect(Object.keys(this.postResponseData)).toContain('ip');
    });

    it('should verify that "token" key is present', function () {
        expect(Object.keys(this.postResponseData)).toContain('token');
    });

    it('should verify that IP is valid', function () {
        expect(isIPv4(this.postResponseData.ip)).toBeTrue();
    });
});
